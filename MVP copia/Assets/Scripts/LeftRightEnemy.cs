﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LeftRightEnemy : MonoBehaviour
{
    public bool goingRight;
    public float velocity;
    public Transform leftDetection;
    public Transform rightDetection;
    public LayerMask groundLayer;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Physics2D.OverlapCircle(rightDetection.position,0.2f,groundLayer)) {
            goingRight = false;
        }
        if(Physics2D.OverlapCircle(leftDetection.position, 0.2f, groundLayer)) {
            goingRight = true;
        }
        if (goingRight==true) {
            transform.Translate(Vector2.right * velocity * Time.deltaTime);
        } else {
            transform.Translate(Vector2.left * velocity * Time.deltaTime);
        }
    }
}
