﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    //SerializeField hace la variable visible en el inspector pero no la hace pública,
    //se pone en corchetes porque es un atributo al igual que [Range]
    [SerializeField] float velocity;
    [SerializeField] float HIinput;
    private Rigidbody2D rb;

    [SerializeField] float jumpForce;

    #region FixedUpdate triggers
    bool doJump;

    #endregion
    bool grounded;

    private bool facingRight = true;

    [SerializeField] Transform groundDetectPos;
    [SerializeField] float groundDetectRadius;
    [SerializeField] LayerMask floorLayer;
    [SerializeField] float maxHMoveSpeed;
    private void Awake() {
        //Cogemos el rigidbody cuando empezamos el juego
        rb = GetComponent<Rigidbody2D>();
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Controls();
    }

    private void FixedUpdate() {
        //Esta linea indica si grounded es true o false, detecta si hay algo chocando con el overlapcircle
        //el overlapcircle es un círculo de detección
        grounded = Physics2D.OverlapCircle(groundDetectPos.position, groundDetectRadius, floorLayer);
        //cuando trabajamos con fisicas y rigidbody el fixedupdate es la opción más recomendada
        //en el fixed update es importante no hacer detecciones de input
        Move();

        ControlsCheck();
    }

    private void Move() {
        //cogemos los botones de izquierda y derecha, de manera brusca (no progresiva) por eso ponemos GetAxisRaw y no GetAxis
        HIinput = Input.GetAxisRaw("Horizontal");
        //estoy cogiendo con esta linea la velocity con el rigidbody y estamos cambiando
        //la velocidad en X y estamos dejando igual la velocidad en Y
        //rb.velocity = new Vector2 (velocity * HIinput * Time.deltaTime, rb.velocity.y);

        if (HIinput>0) {
            if (rb.velocity.x <=maxHMoveSpeed) {
                rb.velocity += new Vector2(velocity * Time.deltaTime, 0);

            }
        }
        if (HIinput<0) {
            if (rb.velocity.x>= -maxHMoveSpeed) {
                rb.velocity += new Vector2(-velocity * Time.deltaTime, 0);

            }
        }

        if (HIinput == 0 && grounded) {
            rb.drag = 6;
        } else {
            rb.drag = 2;
        }

        if(Physics2D.OverlapCircle(groundDetectPos.position, groundDetectRadius, floorLayer) != null) {
            if (Physics2D.OverlapCircle(groundDetectPos.position, groundDetectRadius, floorLayer).CompareTag("Ice")) {
                rb.drag = 0.5f;
            }
        }

        GravityScaleManagement();
    }

    private void Jump() {
        if (grounded==true) {
            rb.AddForce(Vector2.up * jumpForce);
        }
    }

    void Controls() {
        if (Input.GetButtonDown("Jump")) {
            doJump = true;
        }
    }

    void ControlsCheck() {
        if (doJump) {
            doJump = false;
            Jump();

        }
    }

    void Flip() {
        facingRight = !facingRight;
        Vector3 Scaler = transform.localScale;
        Scaler.x *= -1;
        transform.localScale = Scaler;
    }

    private void OnDrawGizmos() {
        Gizmos.DrawWireSphere(groundDetectPos.position, groundDetectRadius);
    }

    void GravityScaleManagement() {
        if(Input.GetButton("Jump")&&rb.velocity.y>0) {
            rb.gravityScale = 3;
        } else {
            rb.gravityScale = 8;
        }
    }

    private void OnTriggerEnter2D(Collider2D collision) {
        if (collision.GetComponent<Spikes>()!=null) {
            GameManager.instance.RestartScene();
        }
    }
}
